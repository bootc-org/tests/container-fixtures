#!/bin/bash
set -eu

# See:
# - https://github.com/osbuild/bootc-image-builder/issues/223
# - https://github.com/ostreedev/ostree-rs-ext/commit/43e1648e97ef82f2cb86fd0813a5f338585eea97
# - https://docs.kernel.org/filesystems/overlayfs.html#index

# Arbitrary file
target=/usr/lib/systemd/system/systemd-modules-load.service
echo "searching for object for $target"
src_inode=$(stat -c '%i' $target)
size=$(stat -c '%s' $target)
find /sysroot/ -size ${size}c -name '*.file' -print | while read line; do
    inode=$(stat -c '%i' $line)
    if test $inode == $src_inode; then
        echo "target object=$line"
        echo $line > /tmp/target
        break
    fi
done
object=$(cat /tmp/target)
if test -z "${object}"; then echo "Failed to find object for $target"; exit 1; fi
flag='# HARDLINK-WRITE-INJECTED'
echo "${flag}" >> ${target}
if ! grep -qFe "${flag}" "${object}"; then
    echo "note: did not write to hardlink target ${object} - forcing relink"
    ln -f ${target} $object
else
    echo "ok overlayfs wrote through hardlink"
fi
